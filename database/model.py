#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3
from config import database_location


class Database():
    def __init__(self):
        self.conn = sqlite3.connect(database_location)
        self.conn.text_factory = str
        self.c = self.conn.cursor()

    def create_table(self):
        self.c.execute('CREATE TABLE articles (id INTEGER PRIMARY KEY ASC, title TEXT NOT NULL, link TEXT NOT NULL, perex TEXT, send INTEGER DEFAULT 0)')
        self.c.execute('CREATE TABLE emails (ID INTEGER PRIMARY KEY ASC, email TEXT NOT NULL)')
        self.conn.commit()

    def mark_all_as_sent(self):
        self.c.execute('UPDATE articles SET send=1 WHERE send=0')
        self.conn.commit()

    def mark_as_sent(self, link):
        self.c.execute('UPDATE articles SET send=1 WHERE link=?', (link,))
        self.conn.commit()

    def insert_new_article(self, title, link, perex):
        self.c.execute('INSERT INTO articles(title, link, perex) VALUES(?,?,?)',(title, link, perex,))
        self.conn.commit()

    def return_last_article(self):
        self.c.execute('SELECT * FROM articles ORDER BY id DESC')
        return self.c.fetchone()

    def return_last_unsend_article(self):
        self.c.execute('SELECT * FROM articles WHERE send=0 ORDER BY id DESC')
        return self.c.fetchone()

    def return_all_articles(self):
        self.c.execute('SELECT * FROM articles')
        return self.c.fetchall()

    def return_all_unsend_articles(self):
        self.c.execute('SELECT * FROM articles WHERE send=0')
        return self.c.fetchall()

    def check_if_link_exist(self, link):
        self.c.execute('SELECT link FROM articles WHERE link=?', (link,))
        if len(self.c.fetchall()) == 0:
            return False
        else:
            return True

    def article_was_send(self, link):
        self.c.execute('SELECT link FROM articles WHERE link=? and send=0', (link,))
        if len(self.c.fetchall()) == 0:
            return True
        else:
            return False

    def get_all_emails(self):
        self.c.execute('SELECT email FROM emails')
        return self.c.fetchall()

    def insert_new_email(self, email):
        self.c.execute('INSERT INTO emails(email) VALUES (?)', (email,))
        self.conn.commit()

    def delete_email(self, email):
        self.c.execute('DELETE FROM emails WHERE email=?', (email,))
        self.conn.commit()

