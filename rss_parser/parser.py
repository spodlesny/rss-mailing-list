#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import feedparser

from config import rss_location


class GetArticles():
    def __init__(self):
        self.rss_location = rss_location
        self.refresh()

    def refresh(self):
        self.feed = feedparser.parse(self.rss_location)
        self.articles = []
        for x in self.feed['entries']:
            self.articles.append(x)

    def get_article(self, position=0):
        self.refresh()
        if len(self.articles) != 0:
                return self.articles[position]
        else:
            print("Error: None article")
            return {}

    def get_title(self, position=0):
        self.refresh()
        return self.articles[position]['title']

    def get_link(self, position=0):
        self.refresh()
        return self.articles[position]['link']

    def get_perex(self, position=0):
        self.refresh()
        return self.articles[position]['summary']

    def get_all_articles(self):
        self.refresh()
        return self.articles

