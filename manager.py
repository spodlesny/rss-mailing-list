#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import sendgrid
import os
from sendgrid.helpers.mail import *
from config import sender, API_key, sender_name
from database.model import Database
from rss_parser.parser import GetArticles
from datetime import datetime

g = GetArticles()
d = Database()

def first_start():
    d.create_table()
    articles = g.get_all_articles()
    file = open("emails/email_addresses", "r")
    for x in articles:
        insert_into_database(x)
    d.mark_all_as_sent()
    for x in file:
        d.insert_new_email(x)



def insert_into_database(article):
    if not d.check_if_link_exist(article['link']):
        #perex using html_encode. You can remove encoding by html.unescape()
        d.insert_new_article(title=article['title'], link=article['link'], perex=article['summary'])
    else:
        return False

def send_newsletter(mailing_list, article):
    # if perex is missing not send message
    sg = sendgrid.SendGridAPIClient(apikey=API_key)
    from_email = Email(sender, sender_name)
    status_summary = []
    subject = article['title']
  
    if article['summary']:
        for email in mailing_list:
            to_email = Email(email)
            content = Content("text/html", article['summary']+"<br> Celý príspevok môžte zobraziť na adrese: "+article['link'])
            mail = Mail(from_email, subject, to_email, content)

            # Will try to send message. Repeat after 60 seconds if fail and then skip address if fail again
            try:
                print("(1) Sending message to: {}".format(email))
                response = sg.client.mail.send.post(request_body=mail.get())
            except Exception as err:
                try:
                    time.sleep(60)
                    print("(1) An error occured. Program slept for 60 seconds and will try to send message again. Error message was {}".format(err))
                    response = sg.client.mail.send.post(request_body=mail.get())
                except Exception as err2:
                    print("(1) Timeout does not help. Skipping address {} and continue. Error message: {}".format(email, err2))

            status_summary.append([email, response.status_code,format(datetime.utcnow())])
    else:
        for email in mailing_list:
            to_email = Email(email)
            content = Content("text/html", "<br> Celý príspevok môžte zobraziť na adrese: "+article['link'])
            mail = Mail(from_email, subject, to_email, content)
            print("Mail to: {} (2)".format(email))

            # Will try to send message. Repeat after 60 seconds if fail and then skip address if fail again
            try:
                print("(2) Sending message to: {}".format(email))
                response = sg.client.mail.send.post(request_body=mail.get())
            except Exception as err:
                try:
                    time.sleep(60)
                    print("(2) An error occured. Program slept for 60 seconds and will try to send message again. Error message was {}".format(err))
                    response = sg.client.mail.send.post(request_body=mail.get())
                except Exception as err2:
                    print("(2) Timeout does not help. Skipping address {} and continue. Error message: {}".format(email, err2))

            status_summary.append([email, response.status_code,format(datetime.utcnow())])
    
    d.mark_as_sent(article['link'])

    print("-------------------------------------------------------------------")

def get_mailing_list():
    emails = d.get_all_emails()
    list_of_emails = []
    for x in emails:
        list_of_emails.append(list(x)[0].replace("\n",""))
    return list_of_emails


if __name__ == "__main__":
    mailing_list = get_mailing_list()
    while True:
        article = g.get_article()
        # sometimes, when server goes offline and back online, get_article() can return empty dict
        if len(article) == 0: continue
        print(article['link'])
        link = article['link']
        if not d.check_if_link_exist(link):
            insert_into_database(article)
            print(("Article " + str(link) + " was inserted into DB"))
            send_newsletter(mailing_list, article)
            print(("Article " + str(link) + " was send"))
            d.mark_as_sent(link)
        else:
            if not d.article_was_send(link):
                send_newsletter(mailing_list, article)
                print(("Article "+str(link)+" was send (2)"))
                d.mark_as_sent(link)
        time.sleep(60) #by default should be 60s
