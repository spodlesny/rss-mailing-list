# Information #
This repository is no longer active. New version is being developed here: https://bitbucket.org/spodlesny/rss-mailing-list-v2












# DESCRIPTION #
This program was created over the weekend as mailing_list module for site, where I do not have access to modify source code.
Program is using RSS feed to get articles and alert people by email whenever a new article was published.
Emails are send by SendGrid service because it's really cool service with great python API.

# CONFIGURATION #
You can set link to RSS, db_name and location in default_config.py
Program use sendgrid API to send emails so you need to have account on https://sendgrid.com/ to get API key.


# INSTALATION #
For a first time, go into mailing_list folder, rename default_config.py to config.py and run "python3 install.py".
This script will create database, import old articles from RSS (also mark them as sent) and import mailing list from "emails/email_addresses" folder.

# START #
After installation, you can run script as:

```
python3 manager.py
```


# NOTE #
Inside Database() class, there is delete_email() method you can use to delete email addresses from database.

# DEPENDENCIES #

```
#!bash

pip3 install sendgrid
pip3 install feedparser
apt-get install sqlite3 libsqlite3-dev
```



# LICENSE #

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.